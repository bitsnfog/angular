/**
 * Created by vaio on 7/5/2016.
 */
var express = require('express');
var mongo = require('mongodb');
var HERO = require('../mean/Hero');
var bodyParser = require('body-parser');
var rm = require('random-number');
var app =  express();

var mongoConnect = mongo.MongoClient, debug = true, hero = HERO, db, col;
var options = {
    min: 10,
    max: 10000000,
    integer: true
}

mongoConnect.connect('mongodb://localhost:27017/test', function(err, db){
    if (err) throw err;
    this.db = db;
    this.col = this.db.collection('bp');
    this.dm('Connected to DB (test) at localhost ');

})

var server = app.listen(8081, function(){
    var servername = server.address().address;
    var serverport = server.address().port;
    console.log('Server - %s listening on port %s',servername,serverport);
} )

app.use( bodyParser.json() );

app.use(function(req, res, next) {

    console.log('Request received - method '+ req.method +" - "+ req.originalUrl);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

app.get('/hero', function(req, res){
            this.col.find().toArray(function (err, result) {
                if (err) {
                    throw err
                }
                else {
                    res.json(result);
                }
            })
   })

app.get('/getByHeroName/:name', function(req, res){
    var user = req.params.name;
    console.log('name passed - %s', user);

            this.col.find({"firstname":user}).toArray(function (err, result){
                if (err)
                     throw err;
                else
                    res.json(result);

            })

    })


app.get('/hero/:id', function(req, res){

    var id = req.params.id;
    this.col.find({_id : id}).toArray(function (err, result){
        if (err) {
            throw err
        }
        else {
            //dm(JSON.toString(result));
            hero = result;
            res.json(result[0]);
        }
    })
})

app.post('/hero', function(req, res){

    hero =  req.body;
    this.col.replaceOne({"_id": hero._id},
        {$set:{"country":hero.country, "firstname":hero.firstname,"lastname":hero.lastname,
            "address":hero.address,"city":hero.city
        }}, (function (err, result) {

        if (err) {
            dm("error  "+err);
            throw err;
        }
        else {
            dm("update successful "+result.ok);
            res.json(hero);
        }
    }));
})

app.delete('/hero/:id', function(req, res){
    hero =  req.body;
    var _id = req.params.id;

    this.col.deleteOne({"_id": _id}, (function (err, result) {

            if (err) {
                dm("error  "+err);
                throw err;
            }
            else {
                dm("update successful "+result.ok);
                res.json(hero);
            }
        }));
})

app.put('/hero', function(req, res){

    hero = req.body;
    var id = rm(options).toString();
    dm (" generated id : "+id);
    hero._id = id;
    dm('hero data : '+JSON.stringify(hero));

    this.col.insertOne(
        {"_id" : id, "country":hero.country, "firstname":hero.firstname,"lastname":hero.lastname,
            "address":hero.address,"city":hero.city
        }, (function (err, result) {

            if (err) {
                dm("error  "+err);
                throw err;
            }
            else {
                dm("update successful ");
                res.json(hero);
            }

        }));
})

dm = function(message) {

    if (debug)
        console.log('ReST - %s : %s  ', Date().slice(3,24), message);

}





