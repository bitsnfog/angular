/**
 * Created by vaio on 7/7/2016.
 */

module.exports = {
    entry: ["./mean/main", "./mean/mean.component"],
    output: {
        filename: "bundle.js"
    },

    module: {

        loaders: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: ['ts', 'angular2-template-loader']

            }
        ]
    },
    resolve: {
        extensions: ['*.ts']
    },

}
