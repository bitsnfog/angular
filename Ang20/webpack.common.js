/**
 * Created by vaio on 7/7/2016.
 */
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
//var helpers = require('./helpers');

module.exports = {
    entry: {
        'polyfills': './mean/polyfills.ts',
        'main': './mean/main.ts',
        'app': './mean/mean.component.ts'
    },

    resolve: {
        extensions: [ '','.js','.ts']
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loaders: ['ts', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: 'html'
            },
        ]
    },

 //   plugins: [
 //      new webpack.optimize.CommonsChunkPlugin({
 //           name: ['mean', 'main','polyfills']
  //     }),

  //     new HtmlWebpackPlugin({
   //        template: 'mean/index.html'
  //      })
   // ]
};
