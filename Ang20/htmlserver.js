/**
 * Created by vaio on 7/5/2016.
 */
var express = require('express');
var app = express();
var url = require('url');

var options = {
    //dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html','js'],
    maxAge: '0',
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('x-timestamp', Date.now());
    }
}

console.log(__dirname);

//mean.use(express.static('mean'));
//mean.use(express.static('/'));
app.use("/", express.static(__dirname));



var server = app.listen(80, function(){
    var servername = server.address().address;
    var serverport = server.address().port;
    console.log('Http Server started @ -%s , Address - http://localhost:%s',Date().slice(0,21),serverport);
} );

app.get('/test', function(req, res){
    res.send('Hello from static server');
    res.end();
})

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ', err);
});
