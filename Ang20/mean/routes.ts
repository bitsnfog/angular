/**
 * Created by vaio on 9/19/2016.
 */


import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeroListComponent }        from './hero-list.component';
import { userDetailComponent}     from './user_detail.component';
import { AddUserComponent }  from './add-user.component'

import './rxjs-operators';

const appRoutes: Routes = [
    {
        path: 'userList',
        component: HeroListComponent
    },
    {
        path: 'userDetail/:id',
        component: userDetailComponent
    },
    {
        path: 'addUser',
        component: AddUserComponent
    },
    {
        path: '',
        component: HeroListComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);