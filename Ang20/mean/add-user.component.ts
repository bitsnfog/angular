/**
 * Created by vaio on 9/19/2016.
 */

/**
 * Created by vaio on 6/30/2016.
 */
import { Component, OnInit } from '@angular/core';
import { Hero }              from './Hero';
import { HeroService} from "./hero.service"
import { Router }            from '@angular/router';


@Component({
    selector: 'addUser',
    templateUrl: 'mean/add-user.component.html',
    providers: [ HeroService ]
})
export class AddUserComponent implements OnInit {

    errorMessage: string;
    messagestatus : boolean = false;
    zero : Hero =  {"_id": "", firstname:"",lastname: "", address : "", city: "", country: "" };


    constructor (private heroService: HeroService) {
       console.log("hero service initialized");
    }

    ngOnInit() {
        this.messagestatus = false;
    }

    addUser(){
        console.log('Update customer : ');
        console.log(this.zero);

        this.heroService.addCustomer(this.zero).then(
            res => {this.zero = res; this.messagestatus = true});
    }

}

