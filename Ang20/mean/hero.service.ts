/**
 * Created by vaio on 9/19/2016.
 */

// Observable Version
import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Hero }           from './Hero';
import { Observable }     from 'rxjs/Observable';
import { HeroUtilities} from './user.utilities'

@Injectable()
export class HeroService {
    constructor (private http: Http, private hu : HeroUtilities) {}

     getCustomer (): Promise<Hero[]> {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service '+ heroesUrl);
        return this.http.get(heroesUrl).toPromise()
            .then(response => response.json() as Hero[])
            .catch(this.handleError);
      }

    getCustomerbyId (id: any): Promise<Hero> {
        var heroesUrl = this.hu.getRestURL()+'/'+id;
        this.hu.dm('get hero service : '+ heroesUrl);
        return this.http.get(heroesUrl).toPromise()
            .then(response => response.json() as Hero)
            .catch(this.handleError);
    }

    updateCustomer (user: Hero): Promise<Hero> {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service : '+ heroesUrl);
        let body = JSON.stringify({ user });
        console.log('Body - '+body);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(heroesUrl, user, options).toPromise()
            .then(response => response.json() as Hero)
            .catch(this.handleError);

    }
    addCustomer (user: Hero): Promise<Hero> {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service : '+ heroesUrl);
        let body = JSON.stringify({ user });
        console.log('Body - '+body);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put(heroesUrl, user, options).toPromise()
            .then(response => response.json() as Hero)
            .catch(this.handleError);

    }


    deleteCustomer (id: any): Promise<any> {
        var heroesUrl = this.hu.getRestURL()+'/'+id;
        this.hu.dm('get hero service : '+ heroesUrl);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(heroesUrl, options).toPromise()
            .then(res => true)
            .catch(this.handleError);
    }



    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }


}

