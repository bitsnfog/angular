import { Component, OnInit }         from '@angular/core';


// Add the RxJS Observable operators we need in this mean.
import './rxjs-operators';
import { HeroUtilities} from './user.utilities';



@Component({
    selector: 'my-app',
    templateUrl: 'mean/app.component.html',
    styleUrls: ['mean/app.component.css']
 })
export class AppComponent implements OnInit {
     title = "";
    constructor (private hu : HeroUtilities) { }

    ngOnInit(){
        if (this.hu.meanExample())
            this.title = 'MEAN Stack example';
        else
            this.title = 'Angular2, Spring, MySQL & Hybernate';
    }
  }
