/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by vaio on 6/30/2016.
 */
var core_1 = require('@angular/core');
var hero_service_1 = require("./hero.service");
var router_1 = require('@angular/router');
var user_utilities_1 = require('./user.utilities');
var HeroListComponent = (function () {
    function HeroListComponent(heroService, router, hu) {
        this.heroService = heroService;
        this.router = router;
        this.hu = hu;
        this.heroes = [{ "_id": '1', "firstname": "Ramana", "lastname": "Kotte", "address": "Address", "city": "city",
                "country": "US" }];
        this.mode = 'Observable';
        this.hu.dm("hero service initialized");
    }
    //constructor (private http: Http) {console.log("http service initialized")};
    HeroListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.hu.dm('Hero List component - init');
        this.heroService.getCustomer().then(function (h) { return _this.heroes = h; });
        //this.http.get('http://localhost:8080/Hero').then(function(response) { this.heroes = response.data });
    };
    /* getHeroes() {
         this.heroService.getCustomer()
             .subscribe(
                 heroes => this.heroes = heroes,
                 error =>  this.errorMessage = <any>error);
     };*/
    HeroListComponent.prototype.gotoDetail = function (st) {
        console.log("goto hero details ", st._id);
        var link = ['/userDetail', st._id];
        console.log('link created %s', JSON.stringify(link));
        this.router.navigate(link);
    };
    HeroListComponent = __decorate([
        core_1.Component({
            selector: 'hero-list',
            templateUrl: 'mean/hero-list.component.html',
            providers: [hero_service_1.HeroService, user_utilities_1.HeroUtilities]
        }), 
        __metadata('design:paramtypes', [hero_service_1.HeroService, router_1.Router, user_utilities_1.HeroUtilities])
    ], HeroListComponent);
    return HeroListComponent;
}());
exports.HeroListComponent = HeroListComponent;
//# sourceMappingURL=hero-list.component.js.map