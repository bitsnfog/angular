/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by vaio on 7/1/2016.
 */
var core_1 = require('@angular/core');
var hero_service_1 = require('./hero.service');
var router_1 = require('@angular/router');
var userDetailComponent = (function () {
    function userDetailComponent(heroService, route) {
        this.heroService = heroService;
        this.route = route;
        // @Input() userId: string;
        this.message = "";
        this.messagestatus = false;
        this.zero = { "_id": "", firstname: "", lastname: "", address: "", city: "", country: "" };
    }
    userDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.messagestatus = false;
        this.sub = this.route.params.subscribe(function (params) {
            if (params['id'] !== undefined) {
                var id = params['id'];
                _this.navigated = true;
                _this.heroService.getCustomerbyId(id).then(function (h) { return _this.dispData(h); });
            }
            else {
                _this.navigated = false;
            }
        });
    };
    userDetailComponent.prototype.updateCustomer = function () {
        var _this = this;
        this.heroService.updateCustomer(this.zero).then(function (res) {
            _this.zero = res;
            _this.message = "Update Successful";
            _this.messagestatus = true;
        });
    };
    userDetailComponent.prototype.deleteCustomer = function () {
        var _this = this;
        console.log(this.zero);
        this.heroService.deleteCustomer(this.zero._id).then(function (res) { _this.messagestatus = res; _this.message = "Delete Succesful"; });
    };
    //goBack(){
    //    window.history.back();
    //}
    userDetailComponent.prototype.dispData = function (x) {
        console.log('display Data');
        console.log('Log Values : ', x.firstname, x.lastname);
        this.zero = x;
    };
    userDetailComponent = __decorate([
        core_1.Component({
            selector: 'userDetail',
            templateUrl: 'mean/hero_detail.component.html',
            providers: [hero_service_1.HeroService]
        }), 
        __metadata('design:paramtypes', [hero_service_1.HeroService, router_1.ActivatedRoute])
    ], userDetailComponent);
    return userDetailComponent;
}());
exports.userDetailComponent = userDetailComponent;
//# sourceMappingURL=user_detail.component.js.map