/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var router_1 = require('@angular/router');
var hero_list_component_1 = require('./hero-list.component');
var user_detail_component_1 = require('./user_detail.component');
var add_user_component_1 = require('./add-user.component');
require('./rxjs-operators');
var appRoutes = [
    {
        path: 'userList',
        component: hero_list_component_1.HeroListComponent
    },
    {
        path: 'userDetail/:id',
        component: user_detail_component_1.userDetailComponent
    },
    {
        path: 'addUser',
        component: add_user_component_1.AddUserComponent
    },
    {
        path: '',
        component: hero_list_component_1.HeroListComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=routes.js.map