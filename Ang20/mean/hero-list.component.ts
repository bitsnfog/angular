/**
 * Created by vaio on 9/19/2016.
 */

/**
 * Created by vaio on 6/30/2016.
 */
import { Component, OnInit } from '@angular/core';
import { Hero }              from './Hero';
import { HeroService} from "./hero.service"
import { Http, Response } from '@angular/http';
import { Router }            from '@angular/router';
import { HeroUtilities} from './user.utilities'

@Component({
    selector: 'hero-list',
    templateUrl: 'mean/hero-list.component.html',
    providers: [ HeroService, HeroUtilities ]
})
export class HeroListComponent implements OnInit {

    errorMessage: string;
    heroes: Hero[] = [ {"_id" :'1', "firstname":"Ramana", "lastname" :"Kotte", "address" : "Address", "city": "city",
        "country" : "US"}];
    mode = 'Observable';

    constructor (private heroService: HeroService, private router: Router, private hu : HeroUtilities) {
       this.hu.dm("hero service initialized");
    }

    //constructor (private http: Http) {console.log("http service initialized")};

    ngOnInit() {
        this.hu.dm('Hero List component - init');
        this.heroService.getCustomer().then(h => this.heroes = h);

        //this.http.get('http://localhost:8080/Hero').then(function(response) { this.heroes = response.data });

    }

   /* getHeroes() {
        this.heroService.getCustomer()
            .subscribe(
                heroes => this.heroes = heroes,
                error =>  this.errorMessage = <any>error);
    };*/

    gotoDetail(st : Hero) {
        console.log("goto hero details ", st._id);
        let link = ['/userDetail', st._id];
        console.log('link created %s', JSON.stringify(link));

        this.router.navigate(link);

    }

}

