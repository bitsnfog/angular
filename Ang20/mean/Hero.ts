export class Hero {
    "_id": string;
    "firstname": string;
    "lastname": string;
    "address": string;
    "city": string;
    "country": string;
}
/**
 * Created by vaio on 9/14/2016.
 */
