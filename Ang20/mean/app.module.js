"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var app_component_1 = require('./app.component');
//import { HeroFormComponent } from './hero-form.component';
var hero_list_component_1 = require('./hero-list.component');
var user_detail_component_1 = require('./user_detail.component');
var add_user_component_1 = require('./add-user.component');
var routes_1 = require("./routes");
var hero_service_1 = require("./hero.service");
var http_1 = require('@angular/http');
var user_utilities_1 = require('./user.utilities');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                routes_1.routing
            ],
            declarations: [
                hero_list_component_1.HeroListComponent,
                app_component_1.AppComponent,
                user_detail_component_1.userDetailComponent,
                add_user_component_1.AddUserComponent
            ],
            providers: [hero_service_1.HeroService, user_utilities_1.HeroUtilities],
            providers: [hero_service_1.HeroService, user_utilities_1.HeroUtilities],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map