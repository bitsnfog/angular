/**
 * Created by vaio on 9/19/2016.
 */

/**
 * Created by vaio on 7/1/2016.
 */

import { Component, OnInit, Input,  Output } from '@angular/core';
import { FormsModule }   from '@angular/forms';


import { Hero }              from './hero';
import { HeroService }       from './hero.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'userDetail',
    templateUrl: 'mean/hero_detail.component.html',
    providers: [ HeroService ]
})
export class userDetailComponent implements OnInit {
    // @Input() userId: string;
    message: string  = "";
    messagestatus: boolean = false;

    zero : Hero =  {"_id": "", firstname:"",lastname: "", address : "", city: "", country: "" };

    constructor (private heroService: HeroService, private route: ActivatedRoute) {}

    ngOnInit(){
        this.messagestatus = false;
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] !== undefined) {
                let id = params['id'];
                this.navigated = true;
                this.heroService.getCustomerbyId(id).then(h => this.dispData(h));
           } else {
                this.navigated = false;
            }
        });
    }

    updateCustomer(){
       this.heroService.updateCustomer(this.zero).then(res =>
                                      {this.zero = res;
                                          this.message = "Update Successful";
                                          this.messagestatus = true} );
    }

    deleteCustomer(){
        console.log(this.zero);
        this.heroService.deleteCustomer(this.zero._id).then(res =>
        {this.messagestatus = res;this.message = "Delete Succesful" });

    }

    //goBack(){
    //    window.history.back();
    //}

    dispData(x: Hero){
        console.log('display Data');
        console.log('Log Values : ',x.firstname, x.lastname);
        this.zero = x;
    }
}


