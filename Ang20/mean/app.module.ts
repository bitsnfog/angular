import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }  from './app.component';
//import { HeroFormComponent } from './hero-form.component';
import { HeroListComponent } from './hero-list.component';
import { userDetailComponent } from './user_detail.component';
import { AddUserComponent }  from './add-user.component'
import {routing} from "./routes";
import { HeroService} from "./hero.service";
import { HttpModule }    from '@angular/http';
import { HeroUtilities} from './user.utilities'

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing

    ],
    declarations: [
        HeroListComponent,
        AppComponent,
        userDetailComponent,
        AddUserComponent
     ],
    providers : [ HeroService, HeroUtilities],
    providers : [ HeroService, HeroUtilities],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
