/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Observable Version
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var user_utilities_1 = require('./user.utilities');
var HeroService = (function () {
    function HeroService(http, hu) {
        this.http = http;
        this.hu = hu;
    }
    HeroService.prototype.getCustomer = function () {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service ' + heroesUrl);
        return this.http.get(heroesUrl).toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    HeroService.prototype.getCustomerbyId = function (id) {
        var heroesUrl = this.hu.getRestURL() + '/' + id;
        this.hu.dm('get hero service : ' + heroesUrl);
        return this.http.get(heroesUrl).toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    HeroService.prototype.updateCustomer = function (user) {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service : ' + heroesUrl);
        var body = JSON.stringify({ user: user });
        console.log('Body - ' + body);
        var headers = new http_2.Headers({ 'Content-Type': 'application/json' });
        var options = new http_2.RequestOptions({ headers: headers });
        return this.http.post(heroesUrl, user, options).toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    HeroService.prototype.addCustomer = function (user) {
        var heroesUrl = this.hu.getRestURL();
        this.hu.dm('get hero service : ' + heroesUrl);
        var body = JSON.stringify({ user: user });
        console.log('Body - ' + body);
        var headers = new http_2.Headers({ 'Content-Type': 'application/json' });
        var options = new http_2.RequestOptions({ headers: headers });
        return this.http.put(heroesUrl, user, options).toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    HeroService.prototype.deleteCustomer = function (id) {
        var heroesUrl = this.hu.getRestURL() + '/' + id;
        this.hu.dm('get hero service : ' + heroesUrl);
        var headers = new http_2.Headers({ 'Content-Type': 'application/json' });
        var options = new http_2.RequestOptions({ headers: headers });
        return this.http.delete(heroesUrl, options).toPromise()
            .then(function (res) { return true; })
            .catch(this.handleError);
    };
    HeroService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    HeroService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, user_utilities_1.HeroUtilities])
    ], HeroService);
    return HeroService;
}());
exports.HeroService = HeroService;
//# sourceMappingURL=hero.service.js.map