/**
 * Created by vaio on 9/19/2016.
 */

// Observable Version
import { Injectable }     from '@angular/core';



@Injectable()
export class HeroUtilities {

    //private baseUrl = '	http://bitsnfog-test.apigee.net/hero';
    private SpringRestUrl = 'http://bitsnfog.ddns.net:8080/hero';
    private NodeRestUrl = 'http://bitsnfog.ddns.net:8081/hero';
    private meanURL = 'mean.html';


    private debug = true;

    public dm(st : any){
        if (this.debug)
            console.log(st);

    }

    private meanExample() : any {
        console.log(window.location);
        if (window.location.pathname.toString().indexOf(this.meanURL) >= 0)
            return true;
        else
            return false;
    }


    public getRestURL() : String {

        if (this.meanExample())
            return this.NodeRestUrl;
        else
            return this.SpringRestUrl;
    }
}

