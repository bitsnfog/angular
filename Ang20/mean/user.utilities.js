/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Observable Version
var core_1 = require('@angular/core');
var HeroUtilities = (function () {
    function HeroUtilities() {
        //private baseUrl = '	http://bitsnfog-test.apigee.net/hero';
        this.SpringRestUrl = 'http://bitsnfog.ddns.net:8080/hero';
        this.NodeRestUrl = 'http://bitsnfog.ddns.net:8081/hero';
        this.meanURL = 'mean.html';
        this.debug = true;
    }
    HeroUtilities.prototype.dm = function (st) {
        if (this.debug)
            console.log(st);
    };
    HeroUtilities.prototype.meanExample = function () {
        console.log(window.location);
        if (window.location.pathname.toString().indexOf(this.meanURL) >= 0)
            return true;
        else
            return false;
    };
    HeroUtilities.prototype.getRestURL = function () {
        if (this.meanExample())
            return this.NodeRestUrl;
        else
            return this.SpringRestUrl;
    };
    HeroUtilities = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], HeroUtilities);
    return HeroUtilities;
}());
exports.HeroUtilities = HeroUtilities;
//# sourceMappingURL=user.utilities.js.map