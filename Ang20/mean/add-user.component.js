/**
 * Created by vaio on 9/19/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by vaio on 6/30/2016.
 */
var core_1 = require('@angular/core');
var hero_service_1 = require("./hero.service");
var AddUserComponent = (function () {
    function AddUserComponent(heroService) {
        this.heroService = heroService;
        this.messagestatus = false;
        this.zero = { "_id": "", firstname: "", lastname: "", address: "", city: "", country: "" };
        console.log("hero service initialized");
    }
    AddUserComponent.prototype.ngOnInit = function () {
        this.messagestatus = false;
    };
    AddUserComponent.prototype.addUser = function () {
        var _this = this;
        console.log('Update customer : ');
        console.log(this.zero);
        this.heroService.addCustomer(this.zero).then(function (res) { _this.zero = res; _this.messagestatus = true; });
    };
    AddUserComponent = __decorate([
        core_1.Component({
            selector: 'addUser',
            templateUrl: 'mean/add-user.component.html',
            providers: [hero_service_1.HeroService]
        }), 
        __metadata('design:paramtypes', [hero_service_1.HeroService])
    ], AddUserComponent);
    return AddUserComponent;
}());
exports.AddUserComponent = AddUserComponent;
//# sourceMappingURL=add-user.component.js.map